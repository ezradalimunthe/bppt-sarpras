window.chartColors = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)'
};

var configChartrequeststate = {
    type: 'pie',
    data: {
        datasets: [{
            data: [
                10,
                14,
                6,
                3
            ],
            backgroundColor: [
                window.chartColors.yellow,
                window.chartColors.purple,
                window.chartColors.green,
                window.chartColors.red
            ],
            label: 'Dataset 1'
        }],
        labels: [
            'Usulan',
            'Diteliti',
            'Ditetapkan',
            'Ditolak'
        ]
    },
    options: {
        responsive: true,
        legend: {
            position: 'right'

        }
    }
};

var configChartAssetState = {
    type: 'pie',
    data: {
        datasets: [{
            data: [
                2548,
                140,
                60
            ],
            backgroundColor: [
                window.chartColors.green,
                window.chartColors.yellow,
                window.chartColors.red
            ],
            label: 'Dataset 1'
        }],
        labels: [
            'Baik',
            'Rusak Ringan',
            'Rusak Berat'
        ]
    },
    options: {
        responsive: true,
        legend: {
            position: 'right'

        }
    }
};
$(function () {

    var crs = $("#chartrequeststate").get(0).getContext("2d");
    new Chart(crs, configChartrequeststate);

    
    var crs = $("#chartassetcondition").get(0).getContext("2d");
    new Chart(crs, configChartAssetState);



})
<?php
namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;

class DashboardInfoBox extends Widget{
    public $sizeClass="col-md-6 col-lg-3";
    public $boxColour = "primary";
    public $icon = "fa-users";
    public $title = "Title";
    public $description="0";

    public function init(){
        parent::init();
    }

    public function run(){
        $result = 
        '<div class="'.$this->sizeClass.'">'.
          '<div class="widget-small '.$this->boxColour.' coloured-icon">'.
            '<i class="icon fa '.$this->icon.'  fa-3x"></i>'.
            '<div class="info">'.
              '<h4>'.$this->title.'</h4>'.
              '<p><b>'.$this->description.'</b></p>'.
            '</div>'.
          '</div>'.
       '</div>';
       return $result;
    }



    
}

/* <div class="col-md-6 col-lg-3">
    <div class="widget-small info coloured-icon"><i class="icon fa fa-thumbs-o-up fa-3x"></i>
    <div class="info">
        <h4>Likes</h4>
        <p><b>25</b></p>
    </div>
    </div>
</div> */
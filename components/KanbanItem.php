<?php
namespace app\components;

use yii\base\Widget;

class KanbanItem extends Widget
{
    public $title="Pengadaan ATK";
    public $subtitle = "31 Januari 2019";
    public function init()
    {
        parent::init();
    }

    public function run()
    {

        
            $result = '<div class="card">
            <div class="card-body">
              <h5 class="card-title">'.$this->title.'</h5>
              <h6 class="card-subtitle mb-2 text-muted">'.$this->subtitle.' <small class="text-muted">(3 hari lalu)</small></h6>
              <p>Unit kerja A</p>
              <div class="clearfix">
                <a class="float-left previewlink" href="#">Preview </a>
                <a class="float-right" href="#">Proses ... <i class="icon fa fa-arrow-right"></i></a>
              </div>
              
            </div>
          </div>';

                    return $result;

    }
}


/* <div class="app-title">
  <div>
    <h1><i class="fa fa-dashboard"></i> <?=$this->title?></h1>
    <p>Start a beautiful journey here</p>
   </div>
  <ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
    <li class="breadcrumb-item"><a href="#"><?=$this->title?></a></li>
  </ul>
</div> */
<?php
use app\components\Breadcrumb;

?>
<?=Breadcrumb::widget(['title' => 'Pencarian', 'icon' => 'fa fa-bar-chart'])?>


<div class="row">
   <div class="col-md-12">
      <div class="tile">
         <div class="row">
            <div class="col-md-8">
                <div style="position:relative;height:500px">
                    <div style="width:100%;height:500px; overflow:hidden">
                        <img   src="../img/street.png" />
                    </div>
                    <div style="width:50px;height:100px; position:absolute;top:20px;right:20px;background-color:white;border-radius:5px;border-color:#c0c0c0;">
                        <div style="width:50px; height:50px;padding-top:10px;" class="text-center"><i class="icon fa fa-plus fa-2x"></i> </div>
                        <div style="width:50px; height:50px;padding-top:10px;" class="text-center"><i class="icon fa fa-minus fa-2x"></i> </div>
                    </div>
                    <div id="car-on-map">
                        <i class="fa fa-car"></i>
                    </div>
                </div>
                
            </div>
                    
            <div class="col-md-4">
                <div class="input-group">
                    <input class="form-control" id="search_mobile_asset" type="text" placeholder="Plat / Kode barang"/>
                    <div class="input-group-append">
                        <button class="app-search__button"><i class="fa fa-search"></i></button>
                    </div>
                </div>
                <ul class="list-group list-group-flush" id='mobile_search_result'>
                    <li class="list-group-item"><i class="fa fa-car"></i> B 1234 XXX</li>
                    <li class="list-group-item"><i class="fa fa-laptop"></i> Laptop 101</li>
                    <li class="list-group-item"><i class="fa fa-car"></i> B 1234 XXX</li>
                    <li class="list-group-item"><i class="fa fa-motorcycle"></i> B 1234 XXX</li>
                    <li class="list-group-item"><i class="fa fa-laptop"></i> Laptop 102</li>
                    <li class="list-group-item"><i class="fa fa-bus"></i> B 1234 XXX</li>
                    <li class="list-group-item"><i class="fa fa-truck"></i> B 1234 XXX</li>
                    
                </ul>
            </div>
            </div>
         
      </div>
   </div>
</div>

<style>
#mobile_search_result li:hover{
    background-color:#c0c0c0;
}
#mobile_search_result li{
    cursor:pointer;
}
#car-on-map{
    position:absolute;
    top:300px;
    left:300px;
    width:30px;
    height:30px;
    background-color:yellow;
    border-radius:50%;
    border:1px solid gray;
    text-align:center;
    line-height:25px;
    vertical-align:middle;

}
</style>


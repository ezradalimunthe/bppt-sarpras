<?php
use app\components\DashboardInfoBox;
use app\components\Breadcrumb;
use app\components\KanbanItem;

$this->registerJsFile(
  '@web/js/chart.min.js',
  ['depends' => [\app\assets\AppAsset::className()]]
);

$this->registerJsFile(
  '@web/js/site/index.js',
  ['depends' => [\app\assets\AppAsset::className()]]
);
?>
<?=Breadcrumb::widget(['title'=>'Dashboard', 'icon'=>'fa fa-bar-chart'])?>

<div class="row">
<?=DashboardInfoBox::widget(
    ['title' => 'Permohonan Belum Diproses', 
    'description' => '20 Dokumen',
    'icon'=>'fa-files-o',
    'sizeClass'=>"col-md-6 col-lg-4"
    ])?>

<?=DashboardInfoBox::widget(
    ['title' => 'Permohonan Dalam Proses', 
    'description' => '5 Dokumen',
    'icon'=>'fa-gears',
    'boxColour'=>'danger',
    'sizeClass'=>"col-md-6 col-lg-4"
    ])?>
<?=DashboardInfoBox::widget(
    ['title' => 'Permohonan Selesai Dan Siap Diserahkan', 
    'description' => '9 Dokumen',
    'icon'=>'fa-gift',
    'boxColour'=>'success',
    'sizeClass'=>"col-md-6 col-lg-4"
    ])?>

</div>

<div class="row">
    <div class="col-lg-4">
        <div class="kanban-line">
          <h3 class="kanban-line-title bg-info text-white">Permohonan Kebutuhan Unit Kerja</h3>
          <div class="kanban-line-body">
                <?php
                  for ($i=0; $i<10; $i++){
                    echo KanbanItem::widget();
                  }
                ?>
                
          </div>
          <div class="kanban-line-footer">
                  <a href="#" class="btn btn-primary">Lainnya...</a>
          </div>
        </div>
    </div>

    <div class="col-lg-4">
    
        <div class="kanban-line">
          <h3 class="kanban-line-title bg-danger text-white">Permohonan Dalam Proses</h3>
          <div class="kanban-line-body">
                <?php
                  for ($i=0; $i<5; $i++){
                    echo KanbanItem::widget();
                  }
                ?>
                
          </div>
        </div>
    </div>
<!--- batas-->
<div class="col-lg-4">
        <div class="kanban-line">
          <h3 class="kanban-line-title bg-success text-white">Siap Untuk Diserahkan</h3>
          <div class="kanban-line-body">
                <?php
                  for ($i=0; $i<9; $i++){
                    echo KanbanItem::widget();
                  }
                ?>
                
          </div>
        </div>
    </div>
    <!-- batas-->


</div>

<!-- modal -->
<div class="modal" id="preview-modal" >
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Pengadaan ATK</h5>
                      <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                          <div class="col-sm-3">Tanggal</div>
                          <div class="col-sm-6"> 31 Januari 2019</div>
                      </div>
                      <div class="row">
                          <div class="col-sm-3">Pemohon</div>
                          <div class="col-sm-6">Unit Kerja</div>
                      </div>
                      <div class="row">
                          <div class="col-sm-12">Rincian Permintaan</div>
                          
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                        <table class="table table-bordered table-sm">
                            <thead>
                                <tr>
                  <th>#</th>
                  <th>Item</th>
                  <th>Kuantitas</th>
                  <th>Satuan</th>
                </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td class='text-right'>1.</td>
                                <td> Kertas A4 80gr </td>
                                <td class='text-right'>30</td>
                                <td> Rim</td>
                              </tr>
                              <tr>
                                <td class='text-right'>2.</td>
                                <td> Buku AAA </td>
                                <td class='text-right'>700</td>
                                <td> buah</td>
                              </tr>
                              <tr>
                                <td class='text-right'>3.</td>
                                <td> Penghapus Pinsil </td>
                                <td class='text-right'>150</td>
                                <td> buah</td>
                              </tr>
                            </tbody>
                </table>


                        </div>
                      </div>


                    </div> <!--end modal-body-->
                    <div class="modal-footer">
                      <button class="btn btn-primary" type="button">Proses</button>
                      <button class="btn btn-secondary" type="button" data-dismiss="modal">Tutup</button>
                    </div>
                  </div>
                </div>
              </div>
<!-- modal end-->

<style>
  .kanban-line{
    position:relative;
    background:#ffffff;
    border-radius:3px;
  	-webkit-box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);
	  box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);
	  margin-bottom: 30px;
	  -webkit-transition: all 0.3s ease-in-out;
	  -o-transition: all 0.3s ease-in-out;
	  transition: all 0.3s ease-in-out;
    margin-left:-10px;
    margin-right:-10px;
    margin-bottom:30px;
  }
  .kanban-line-title{
    margin-top:0;
    margin-bottom:10px;
    font-size:1.25rem;
    padding:10px 20px;
    border-radius:5px 5px 0 0;

  }
  .kanban-line-body{
    min-height:100px;
    padding:5px 10px 10px 10px;
  }
  .kanban-line-footer{
    margin-bottom:0px;
    font-size:1.25rem;
    padding:10px 20px;
  }
  .kanban-line .card{
    margin-bottom:20px;
  }

 
</style>

